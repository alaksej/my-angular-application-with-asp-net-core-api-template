import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from '../environments/environment.prod';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'app';
  values$: Observable<any>;

  constructor(private http: HttpClient) { }

  ngOnInit(): void {
    this.values$ = this.http.get(`${environment.server}${environment.apiUrl}values`);
  }
}
