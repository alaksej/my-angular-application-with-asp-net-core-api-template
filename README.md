# Visual Studio template for Angular with ASP.NET Core Web Api
## Project setup workflow
Prerequisites:
* Visual Studio
* Git
* Node.js + NPM
* Angular cli (`npm i -g @angular/cli`)
            
* .NET Core


1. Open Visual Studio
1. Add blank solution (with a Git repository)
 
    ![Add blank solution](screenshots/add-blank-solution.png)

1. Add Angular app:
    1. Add an empty project

    ![Add empty project for Angular](screenshots/add-empty-project-for-angular.png)

    1. Clean up all the files from the empty project

    ![Clean up files](screenshots/cleanup-files-from-empty-project.png)

    1. Generate an Angular application with angular CLI
        ```bash
        ng new my-angular-app --routing --style scss
        ```
    1. Move the just generated files one level up the folder structure and include them in the project so that there is the following folder structure of the Web project:

        ![Folder structure for the Web project](screenshots/folder-structure-web-project.png)

1. Add ASP.NET Core WebApi app:
    * Using Visual Studio:

    ![Add ASP.NET Core WebAPI - step 1](screenshots/add-asp-net-core-webapi-project.png)

    ![Add ASP.NET Core WebAPI - step 2](screenshots/add-asp-net-core-webapi-project2.png)

    * Or using dotnet cli:
        ```bash
        mkdir MyCompany.MyProject.WebAPI
        cd MyCompany.MyProject.WebAPI
        dotnet new
        ```

### Notes
* The package-lock.json file is generated after 'npm install' and [should be checked into source control](https://stackoverflow.com/questions/44206782/do-i-commit-the-package-lock-json-file-created-by-npm-5)

## Development and production environments setup
1. Set the port for WebAPI
    1. Check out launchSettings.json (if the file does not exist the default port is 5000). 
1. In the Angular project:
    1. For dev env: edit environment.ts - add config here, e.g.:
    ```javascript
    export const environment = {
        production: true,
        server: 'http://localhost:62510/',
        apiUrl: 'api/'
    };
    ```
    1. For prod env: edit environment.prod.ts - add config here according to your prod hosting setup. See [How to host angular](https://www.blinkingcaret.com/2018/01/24/angular-and-asp-net-core/).
1. In the ASP.NET Core WebAPI project:
    1. Set up CORS:
        1. Add the following to ConfigureServices method (Startup.cs)
        ```csharp
            services.AddCors();
        ```
        1. Add the following to Configure method (Startup.cs)

        ```csharp
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseCors(builder => builder.WithOrigins("http://localhost:4200"));
            }
            else
            {
                app.UseCors(builder => builder.WithOrigins("http://localhost")); // or other server
            }
        ```

Test the setup by adding a backend call to the Angular application.

## Running the application

### Development
1. Start backend
```bash
cd MyCompany.MyProject.WebAPI
dotnet run
```
1. Start frontend
```bash 
cd MyCompany.MyProject.Web
ng serve
```
1. Open http://localhost:4200

### Debugging backend with VS Code
1. Install 'C# for Visual Studio Code (powered by OmniSharp)' extension for VS Code
1. In VSCode open debugging tab and select 'Add configuration'
1. In the default configuration add the following to "env" section:
```json
"ASPNETCORE_URLS": "http://localhost:<backendPort>/"
```

### Linting
* `ng lint`
* Install 'TSLint' extension for VS Code

### Testing
* `ng test`
* If launching Chrome fails with error 'No captured browser...' try the following config in karma.conf.js:
```javascript
    browsers: ['ChromeNoSandbox'],
    customLaunchers: {
      ChromeNoSandbox: {
        base: 'Chrome',
        flags: ['--no-sandbox']
      }
    },
```

## Useful links

* [How to host angular](https://www.blinkingcaret.com/2018/01/24/angular-and-asp-net-core/)
* [IIS configuration](https://docs.microsoft.com/en-us/aspnet/core/host-and-deploy/iis/index?view=aspnetcore-2.0&tabs=aspnetcore2x)